from flask import Flask
from dotenv import load_dotenv
import os

load_dotenv()


app = Flask(__name__)

@app.route('/')
def home():
    return " Welcome Greeshma Saseendran"

@app.route('/dashboard')
def dashboard():
    #mock API credentials
    api_token = os.getenv('API_TOKEN' , ' MySecureToken')
    api_url = os.getenv('API_URL' , 'https://fakeweatherservice.com/getforecast')
    return f"APITOKEN: {api_token}" \
           f"\nAPI_URL: {api_url}"

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=6001)



